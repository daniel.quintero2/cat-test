import * as Elm from '../helpers/crear_elementos.js';
import * as GestorGatos from '../services/service_gatos.js';
import { Gato } from '../models/gato.js';

export const mostrarGatos = (gatos) => {
    ocultarGatos();
    gatos.map((gato, index) => {
        mostrarGato(gato);
    });
}

export const mostrarRazas = (razas) => {
    razas.map((raza, index) => {
        document.querySelector("#container-razas").appendChild(mostrarRaza(raza, true));
    });    
}

export const ocultarGatos = () => {
    document.querySelector("#container-gatos").innerHTML = '';
}

export const mostrarGato = (gato) => {
    if(!gato) return false;
    let containerGato = Elm.crearElemento('div', { 
            class: 'container-gato row', id: 'gato-' + gato.getId()
        },
        [
            Elm.crearElemento("div", {class: "col-sm-1"}, [
                Elm.crearElemento("label", { class: 'gato-id rounded-circle' }, [gato.getId() || ""])
            ]),
            Elm.crearElemento("div", {class: "col-sm-11 gato-content"}, [
                Elm.crearElemento("div", { class: 'div col-sm-7' }, [
                    Elm.crearElemento("div", { class: 'div col-sm-12 gato-nombre' }, [
                        Elm.crearElemento("label", {}, [gato.getNombre() || ""])
                    ]),                        
                    Elm.crearElemento("div", { class: 'col-sm-12' }, [
                        Elm.crearElemento("label", {}, [`Edad:  ${gato.getEdad()? gato.getEdad() + " años" : "-"}`]),
                    ]),
                    Elm.crearElemento("div", {class: "col-sm-12 gato-eliminar"}, [
                        btnEliminar(gato.getId())
                    ]),
                ]),
                mostrarRaza(GestorGatos.RAZAS.find(r => r.id == gato.getRaza()) || {}, false),  
            ]),            
        ]
    );
    
    document.querySelector("#container-gatos").appendChild(containerGato);
}

export const mostrarRaza = (raza, inputCheckBool) => {
    let containerRaza = Elm.crearElemento('div', { class: 'container-raza text-center col-sm-12', id: 'raza-' + raza.id },
        [
            Elm.crearElemento("div", {class: 'row raza-nombre'}, [
                Elm.crearElemento("div", { class: 'col-sm-12'}, [
                    Elm.crearElemento("label", {}, [ raza.nombreRaza || ""]),
                    (inputCheckBool)? Elm.crearElemento("input", { type: 'radio', name: 'raza', value: raza.id, class: 'form-check-input raza-radio' }, [raza.id]) : '',
                ]),               
            ]),
            Elm.crearElemento("div", { class: 'col-sm-12'}, [
                Elm.crearElemento("img", { src: raza?.img || "", class: 'raza-img mw-100', alt: 'Raza' }, [])
            ]),
        ]
    );
    
    return containerRaza;
}

const btnEliminar = (id) => {
    let btnEliminar = Elm.crearElemento("label", {}, ['Botar']);
    btnEliminar.addEventListener("click", (e)=> { 
        let gatoPorBotar = GestorGatos.buscarGatoPorID(id);
        if(confirm(`¡¿En serio quieres botar a ${gatoPorBotar.getNombre()}?!`)) {
            alert(GestorGatos.botarGato(gatoPorBotar)?
            `${gatoPorBotar.getNombre()} ha sido botado :C` : `${gatoPorBotar.getNombre()} no ha sido encontrado`)
        }

        mostrarGatos(GestorGatos.GATOS);
        document.querySelector("#input-buscar-gato").value = '';
    });
    return btnEliminar;
}