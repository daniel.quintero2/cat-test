import * as GestorGatos from '../services/service_gatos.js';
import * as VW from './view_gatos.js'
import './gatos.scss';

//alert("Gitlab CI/CD - En proceso!");
GestorGatos.obtenerRazas().then((razas) => {
    // console.log(razas);
    VW.mostrarRazas(razas);
    VW.mostrarGatos(GestorGatos.GATOS);
});

//Evento para buscar gatos por nombre:
document.querySelector("#input-buscar-gato").addEventListener("keydown", (e)=> { 
    VW.ocultarGatos();
    setTimeout(()=>{
        VW.mostrarGatos(GestorGatos.buscarGatosPorNombre(document.getElementById("input-buscar-gato").value))
    }, 100);
});

//Evento para recoger un nuevo gato:
document.querySelector("#form-recoger-gato").addEventListener("submit", (e)=> { 
    e.preventDefault();
    let form = document.querySelector("#form-recoger-gato");
    let gatoRecogido = GestorGatos.recogerNuevoGato(
        {
            nombre: form.querySelector("#nombre").value,
            edad: form.querySelector("#edad").value,
            raza: form.querySelector("input[name='raza']:checked")?.value,
        }
    );
    VW.mostrarGato(gatoRecogido);
    // console.log("Nuevo gato recogido: ", gatoRecogido);
});

//Evento para botar todos los gatos:
document.querySelector("#btn-botar-gatos").addEventListener("click", (e) => {
    GestorGatos.botarTodosLosGatos();
    VW.mostrarGatos(GestorGatos.GATOS);
    document.querySelector("#input-buscar-gato").value = '';
});