import { Gato } from '../models/gato.js'
export let GATOS = [];
export let RAZAS = new Map();

export async function obtenerRazas() { 
    RAZAS = await fetch('http://r98r6.mocklab.io/cats').then(response => {
        if(response.ok) {
            return response.json();
        }
        else return [];
    }).catch(e => {
        throw new Error(`Error al intentar obtener las razas: ${e}`);
    });
    return RAZAS;
}

export const recogerNuevoGato = (nuevoGato) => {
    let gatoRecogido = new Gato(nuevoGato);
    let ultimoGato = GATOS[GATOS.length-1];
    gatoRecogido.id = GATOS.length + 1;
    GATOS.push(gatoRecogido);
    return gatoRecogido;
}

export const buscarGatoPorID = (IDGatoBuscado) => {
    return GATOS.find(g => g?.id === IDGatoBuscado)? 
        GATOS.find(g => g?.id === IDGatoBuscado) : `Gato con ID ${IDGatoBuscado} no encontrado`;
}

export const buscarGatosPorNombre = (nombreGatosBuscados) => {
    return GATOS.filter((g) => {
        return g.nombre.toLowerCase().match(nombreGatosBuscados.toLowerCase());
    });
}

export const botarGato = (gatoPorBotar) => { 
    let indexGatoPorBotar = GATOS.indexOf(gatoPorBotar);
    if(indexGatoPorBotar !== -1) {
        GATOS[indexGatoPorBotar] = undefined;
        return true;
    } else return false;
}

export const botarTodosLosGatos = () => { 
    GATOS = [];    
}