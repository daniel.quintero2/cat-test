export class Gato {
    constructor(gato) {
        (gato)? gato && Object.assign(this, gato) : false;
        // this.id = gato.id;
    }

    getId() { return this.id }
    getNombre() { return this.nombre }
    getEdad() { return this.edad }
    getRaza() { return this.raza }
}