// const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const CompressionPlugin = require("compression-webpack-plugin");

const javaScriptRules = {
    test: /\.js$/,
    exclude: /node_modules/,
    use: {
        loader: 'babel-loader',
        options: {
            presets: ['@babel/preset-react', '@babel/preset-env']
        }
    }
}

const sassRules = {
    test: /\.s[ac]ss$/i,
    use: [        
        'style-loader',// Creates `style` nodes from JS strings
        'css-loader',// Translates CSS into CommonJS
        {
            loader: 'sass-loader',// Compiles Sass to CSS
            options: {
                implementation: require('sass')
            }
        }
    ]
}

const productionPlugins = [
    new CompressionPlugin()
]

module.exports = (env, {mode}) => ({
    entry: {
        index: './src/index.js',
        gatos: './src/gatos/gatos.js'
    },
    output: {
        //path: path.resolve(__dirname, 'dist'),
        filename: '[name].[hash].js' 
    },
    module: {
        rules: [sassRules] //javaScriptRules
    },
    plugins: [
        mode === 'production' && [...productionPlugins], //mode === 'development': false
        //mode === 'development' && [...developmentPlugins],
        new HtmlWebpackPlugin({
            template: 'src/index.html',
            inject: true,
            chunks: ['index'],
            filename: 'index.html',
        }),
        new HtmlWebpackPlugin({
            template: 'src/gatos/gatos.html',
            inject: true,
            chunks: ['gatos'],
            filename: 'gatos.html',
        }),
    ].filter(Boolean) //no carga los que estén en false
})
//[hash]: hashes: Definir en la configuración del bundelizado que se pueda detectar qué hash se ha hecho a través del contenido. Esto debido a que si quiero cachear cada status, por ejemplo si hago un deploy a las 5 y otro a las 6 y lo cacheo para siempre, el usuario del 2do deploy no va a ver los cambios. Necesitamos que cada estático tenga un hash único para evitar que hayan usuarios que aún vean la versión anterior


// const path = require('path');

// module.exports = {
//   mode: 'development',
//   entry: './src/index.js',
//   output: {
//     filename: 'bundle.js',
//     path: path.resolve(__dirname, 'public'),
//   },
//   module: {
//     rules: [
//       {
//         test: /\.m?js$/,
//         exclude: /node_modules/,
//         use: {
//           loader: "babel-loader",
//           options: {
//             presets: ["@babel/preset-env", "@babel/preset-react"]
//           }
//         }
//       }
//     ]
//   }
// };